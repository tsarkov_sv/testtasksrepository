﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PravoRu.Tests.Utils
{
    [TestClass]
    public class BookUtils
    {
        [TestMethod]
        public void TopReport()
        {
            var _response = PravoRu.Utils.BookUtils.TopReport();
            Assert.IsNotNull(_response);
        }

        [TestMethod]
        public void GetBooks()
        {
            var _response = PravoRu.Utils.BookUtils.GetBooks(null);
            Assert.IsNotNull(_response);
            _response = PravoRu.Utils.BookUtils.GetBooks(new Models.BookFilter() { Name = null, Search = null});
            Assert.IsNotNull(_response);
            _response = PravoRu.Utils.BookUtils.GetBooks(new Models.BookFilter() { Name = "", Search = null });
            Assert.IsNotNull(_response);
            _response = PravoRu.Utils.BookUtils.GetBooks(new Models.BookFilter() { Name = "", Search = "" });
            Assert.IsNotNull(_response);
            _response = PravoRu.Utils.BookUtils.GetBooks(new Models.BookFilter() { Name = "Name", Search = "" });
            Assert.IsNotNull(_response);
            _response = PravoRu.Utils.BookUtils.GetBooks(new Models.BookFilter() { Name = "Name", Search = null });
            Assert.IsNotNull(_response);
            _response = PravoRu.Utils.BookUtils.GetBooks(new Models.BookFilter() { Name = "Author", Search = null });
            Assert.IsNotNull(_response);
            _response = PravoRu.Utils.BookUtils.GetBooks(new Models.BookFilter() { Name = "Name", Search = "Test" });
            Assert.IsNotNull(_response);
            _response = PravoRu.Utils.BookUtils.GetBooks(new Models.BookFilter() { Name = "Author", Search = "Dal" });
            Assert.IsNotNull(_response);
        }

        [TestMethod]
        public void GetStat()
        {

        }
    }
}
