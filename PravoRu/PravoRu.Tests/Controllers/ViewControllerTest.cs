﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PravoRu.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PravoRu.Tests.Controllers
{
    [TestClass]
    public class ViewControllerTest
    {
        //Данный тест показывает, что при отправке null в метод Char ViewController возникает ошибка(Специально оставлено так)
        [TestMethod]
        public void Chart()
        {
            // Arrange
            ViewController controller = new ViewController();
            // Act
            ViewResult result = controller.Chart(null) as ViewResult;
            // Assert
            Assert.IsNotNull(result);
        }
    }
}
