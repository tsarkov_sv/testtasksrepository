﻿using PravoRu.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PravoRu.Utils
{
    public class SubsUtils
    {
        public static List<Models.Subscribers> GetSubscribers()
        {
            MvcApplication.Logger.Trace($"-> SubsUtils.GetSubscribers()");
            System.Diagnostics.Stopwatch stopwatch = System.Diagnostics.Stopwatch.StartNew();
            List<Subscribers> _response = new List<Subscribers>();
            try
            {
                using (DBContext _context = new DBContext())
                {
                    _response= _context.Database.SqlQuery<Subscribers>("select * from Subscribers").ToList();
                }
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception);
            }
            MvcApplication.Logger.Trace($"<- GetSubscribers ({stopwatch.ElapsedMilliseconds}мс)");
            return _response;
        }
    }
}