﻿using Newtonsoft.Json;
using PravoRu.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PravoRu.Utils
{
    public class BookUtils
    {
        public static List<Book> TopReport()
        {
            MvcApplication.Logger.Trace($"-> BookUtils.TopReport()");
            System.Diagnostics.Stopwatch stopwatch = System.Diagnostics.Stopwatch.StartNew();
            List<Book> _response = new List<Book>();
            try
            {
                using (DBContext _context = new DBContext())
                {
                    _response = _context.Database.SqlQuery<Book>(@"select b.BookName as Name,tbl.[count] as [Count] from (
                    select top 10 Count(1) as  [count], b.BookId from BooksInUses b
                    group by b.BookId
                    order by[count] desc) tbl
                    inner join Books b on tbl.BookId = b.Id").ToList();
                }
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception);
            }
            MvcApplication.Logger.Trace($"<- TopReport ({stopwatch.ElapsedMilliseconds}мс)");
            return _response;
        }

        public static List<Book> GetBooks(BookFilter Filter)
        {
            MvcApplication.Logger.Trace($"-> BookUtils.GetBooks({JsonConvert.SerializeObject(Filter)})");
            System.Diagnostics.Stopwatch stopwatch = System.Diagnostics.Stopwatch.StartNew();
            List<Book> _response = new List<Book>();
            try
            {
                using (DBContext _context = new DBContext())
                {
                    if (Filter == null)
                    {
                        _response = _context.SearchBooks("", "");
                    }else
                    {
                        _response = _context.SearchBooks(Filter.Name, Filter.Search);
                    }
                }
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception);
            }
            MvcApplication.Logger.Trace($"<- Books ({stopwatch.ElapsedMilliseconds}мс)");
            return _response;
       }

        public static List<Tuple<string, int>> GetStat(DateTime StartDate, DateTime EndDate)
        {
            MvcApplication.Logger.Trace($"-> BookUtils.GetStat(StartDate:{StartDate.ToShortDateString()},EndDate:{EndDate.ToShortDateString()})");
            System.Diagnostics.Stopwatch stopwatch = System.Diagnostics.Stopwatch.StartNew();
            List<Tuple<string, int>> _response = new List<Tuple<string, int>>();
            try
            {
                using (DBContext _context = new DBContext())
                {
                    var _list = _context.Database.SqlQuery<StatModel>($@"select count(1) as Item2,StartDate as Item1 from BooksInUses
                    where StartDate between @StartDate and @EndDate
                    group by StartDate", new SqlParameter("StartDate", StartDate), new SqlParameter("EndDate", EndDate)).ToList();
                    foreach (var _l in _list)
                    {
                        _response.Add(new Tuple<string, int>(_l.Item1.ToString("dd.MM.yyyy"), _l.Item2));
                    }
                }
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception);
            }
            MvcApplication.Logger.Trace($"<- GetStat ({stopwatch.ElapsedMilliseconds}мс)");
            return _response;
        }
    }
}