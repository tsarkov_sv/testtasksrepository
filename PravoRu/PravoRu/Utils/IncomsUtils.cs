﻿using PravoRu.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PravoRu.Utils
{
    public class IncomsUtils
    {
        public static List<Incomings> GetIncomings()
        {
            MvcApplication.Logger.Trace($"-> IncomsUtils.GetIncomings()");
            System.Diagnostics.Stopwatch stopwatch = System.Diagnostics.Stopwatch.StartNew();
            List<Incomings> _response = new List<Incomings>();
            try
            {
                using (DBContext _context = new DBContext())
                {
                    _response = _context.Database.SqlQuery<Incomings>(@"
                    select b.BookName as Book,b.Count as [Count],i.Date,i.Id, p.Name as Provider from Incomings i
                    left join Books b on i.BookId = b.Id
                    left join Providers p on p.Id = i.ProviderId").ToList();
                }
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception);
            }
            MvcApplication.Logger.Trace($"<- GetIncomings ({stopwatch.ElapsedMilliseconds}мс)");
            return _response;
        }
    }
}