namespace PravoRu.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CountInUse : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Books", "Count", c => c.Int(nullable: false));
            AddColumn("dbo.Books", "InUse", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Books", "InUse");
            DropColumn("dbo.Books", "Count");
        }
    }
}
