namespace PravoRu.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangePicType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Books", "Pic", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Books", "Pic", c => c.Binary());
        }
    }
}
