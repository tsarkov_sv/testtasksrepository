﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace PravoRu.Models
{
    public class ChartFilter
    {
        public DateTime? StartDate
        {
            get
            {
                if (String.IsNullOrWhiteSpace(StartDateStr))
                {
                    return null;
                }
                else {
                    try
                    {
                        return DateTime.ParseExact(StartDateStr, "dd.MM.yyyy", CultureInfo.InvariantCulture);
                    }
                    catch (Exception e)
                    {
                        MvcApplication.Logger.Error(e);
                        return null;
                    }
                }
            }
            set
            {
                StartDateStr = value?.ToString("dd.MM.yyyy");
            }
        }
        public DateTime? EndDate
        {
            get
            {
                if (String.IsNullOrWhiteSpace(EndDateStr))
                {
                    return null;
                }
                else {
                    try
                    {
                        return DateTime.ParseExact(EndDateStr, "dd.MM.yyyy", CultureInfo.InvariantCulture);
                    }
                    catch (Exception e)
                    {
                        MvcApplication.Logger.Error(e);
                        return null;
                    }
                }
            }
            set
            {
                EndDateStr = value?.ToString("dd.MM.yyyy");
            }
        }
        public string StartDateStr { get; set; }
        public string EndDateStr { get; set; }
    }
}