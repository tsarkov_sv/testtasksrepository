﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PravoRu.Models
{
    public class IndexModel
    {
        public List<Book> Books { get; set; } = new List<Book>();
        public List<Subscribers> Subscribers { get; set; } = new List<Models.Subscribers>();
        public List<Incomings> Incomings { get; set; } = new List<Models.Incomings>();
    }
}