﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PravoRu.Models
{
    public class Incomings
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Provider { get; set; }
        public string Book { get; set; }
        public int Count { get; set; }
    }
}