﻿using System.Data.Entity;
using PravoRu.Models.DB;
using System.Collections.Generic;
using System.Linq;

namespace PravoRu.Models
{
    public class DBContext: DbContext
    {
        public DBContext():base("DbConnection")
        {
        }

        public DbSet<Models.DB.Author> Authors { get; set; }
        public DbSet<Models.DB.Book> Books { get; set; }
        public DbSet<Models.DB.BooksInUse> BooksInUses { get; set; }
        public DbSet<Models.DB.Incoming> Incomings  { get; set; }
        public DbSet<Models.DB.Provider> Providers { get; set; }
        public DbSet<Models.DB.PublishingHouse> PublishingHouses { get; set; }
        public DbSet<Models.DB.Subscriber> Subscribers { get; set; }
        public DbSet<Models.DB.BookAuthor> BookAuthors { get; set; }
        public DbSet<Models.DB.BookGenre> BookGenres { get; set; }
        public DbSet<Models.DB.Genre> Genres { get; set; }

        public virtual List<Book> SearchBooks(string Name,string Value)
        {
            return this.Database.SqlQuery<Book>($"SELECT * FROM [dbo].[SearchBooks](N'{Name ?? ""}',N'{Value ?? ""}')").ToList();
        }
    }
}