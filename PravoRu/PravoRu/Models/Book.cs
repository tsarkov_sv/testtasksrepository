﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PravoRu.Models
{
    public class Book
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Genre { get; set; }
        public int Count { get; set; }
        public string Pic { get; set; }
        public int InUse { get; set; }
        public string Authors { get; set; }
    }
}