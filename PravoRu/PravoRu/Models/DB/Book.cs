﻿namespace PravoRu.Models.DB
{
    public class Book
    {
        public int Id { get; set; }
        public int PublishingHouseId { get; set; }
        public string Edition { get; set; }
        public string ISBN { get; set; }
        public string BookName { get; set; }
        public string ShortDescription { get; set; }
        public string Pic { get; set; }

        public int Count { get; set; }
        public int InUse { get; set; }

        public virtual PublishingHouse PublishingHouse { get; set; }
    }
}