﻿using System;

namespace PravoRu.Models.DB
{
    public class Subscriber
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}