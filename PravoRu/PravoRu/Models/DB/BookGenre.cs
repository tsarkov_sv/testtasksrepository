﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PravoRu.Models.DB
{
    public class BookGenre
    {
        public int Id { get; set; }
        public int GenreId { get; set; }
        public int BookId { get; set; }

        public virtual Book Book { get; set; }
        public virtual Genre Genre{ get; set; }
    }
}