﻿using System;

namespace PravoRu.Models.DB
{
    public class BooksInUse
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int SubscriberId { get; set; }
        public int BookId { get; set; }
        public bool IsReturned { get; set; }

        public virtual Book Book { get; set; }
        public virtual Subscriber Subscriber { get; set; }
    }
}