﻿namespace PravoRu.Models.DB
{
    public class Provider
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}