﻿using System;

namespace PravoRu.Models.DB
{
    public class Incoming
    {
        public int Id { get; set; }
        public int BookId { get; set; }
        public int ProviderId { get; set; }
        public int Count { get; set; }
        public DateTime Date { get; set; }

        public virtual Book Book { get; set; }
        public virtual Provider Provider { get; set; }
    }
}