﻿namespace PravoRu.Models.DB
{
    public class PublishingHouse
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}