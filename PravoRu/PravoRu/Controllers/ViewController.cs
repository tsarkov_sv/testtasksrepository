﻿using Newtonsoft.Json;
using PravoRu.Models;
using PravoRu.Models.DB;
using PravoRu.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PravoRu.Controllers
{
    public class ViewController : Controller
    {
        [HttpPost]
        public ActionResult Books(BookFilter Filter)
        {
            System.Diagnostics.Stopwatch stopwatch = System.Diagnostics.Stopwatch.StartNew();
            MvcApplication.Logger.Trace($"-> Books ({JsonConvert.SerializeObject(Filter)})");
            List<Models.Book> _list = new List<Models.Book>();
            try
            {
                _list = BookUtils.GetBooks(Filter);
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception);
            }
            MvcApplication.Logger.Trace($"<- Books ({stopwatch.ElapsedMilliseconds}мс)");
            return PartialView("Books",_list);
        }

        [HttpPost]
        public ActionResult Top()
        {
            System.Diagnostics.Stopwatch stopwatch = System.Diagnostics.Stopwatch.StartNew();
            MvcApplication.Logger.Trace($"-> Top ()");
            List<Models.Book> _list = new List<Models.Book>();
            try
            {
                _list = BookUtils.TopReport();
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception);
            }
            MvcApplication.Logger.Trace($"<- Top ({stopwatch.ElapsedMilliseconds}мс)");
            return PartialView("Top", _list);
        }


        //Оставил специально так метод, чтобы показать, что UnitTest находит ошибку
        [HttpPost]
        public ActionResult Chart(ChartFilter Filter)
        {
            System.Diagnostics.Stopwatch stopwatch = System.Diagnostics.Stopwatch.StartNew();
            MvcApplication.Logger.Trace($"-> Chart ({JsonConvert.SerializeObject(Filter)})");
            List<Tuple<string, int>> _response = new List<Tuple<string, int>>();
            try
            {
                if (!Filter.StartDate.HasValue)
                    Filter.StartDate = DateTime.Now.AddYears(-20); ;
                if (!Filter.EndDate.HasValue)
                    Filter.EndDate = DateTime.Now.AddYears(20);
                _response = BookUtils.GetStat(Filter.StartDate.Value, Filter.EndDate.Value);
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception);
            }
            MvcApplication.Logger.Trace($"<- Chart ({stopwatch.ElapsedMilliseconds}мс)");
            return PartialView("Chart",_response);
        }
    }


}