﻿using Newtonsoft.Json;
using PravoRu.Models;
using PravoRu.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PravoRu.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            MvcApplication.Logger.Trace($"-> HomeController.Index");
            System.Diagnostics.Stopwatch stopwatch = System.Diagnostics.Stopwatch.StartNew();
            IndexModel _model = new IndexModel();
            try
            {
                _model.Books = BookUtils.GetBooks(null);
                _model.Incomings = IncomsUtils.GetIncomings();
                _model.Subscribers = SubsUtils.GetSubscribers();
            }
            catch (Exception exception)
            {
                MvcApplication.Logger.Error(exception);
            }
            MvcApplication.Logger.Trace($"<- Books ({stopwatch.ElapsedMilliseconds}мс)");
            return View(_model);
        }
    }
}