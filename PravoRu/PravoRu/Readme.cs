﻿/*

select 
book.*,
ph.Name as PublishingHouse,
(select
stuff((
		select ','+g.Name from BookGenres bg 
		left join Genres g on bg.GenreId=g.Id 
		where bg.BookId=book.Id
		FOR XML path('')
    ),1,1,'')) as Genre,
(select
	stuff((
		select ','+a.Name from BookAuthors ba 
		left join Authors a on ba.AuthorId=a.Id 
		where ba.BookId=book.Id
		FOR XML path('')
    ),1,1,'')) as Author
from Books book
left join PublishingHouses ph on book.PublishingHouseId=ph.Id


CREATE FUNCTION [dbo].[SearchBooks]
(
	@Name nvarchar(250),
	@Author nvarchar(250)
)
RETURNS @returntable TABLE
(
	Id int,
	Name nvarchar(250),
	Genre nvarchar(250),
	[Count] int,
	Pic varbinary(max),
	InUse int,
	Authors nvarchar(250)
)
AS
BEGIN
	INSERT @returntable (Id,Name,[Count],Pic,InUse,Genre,Authors)
	select 
	book.Id,
	book.BookName,
	book.[Count],
	book.Pic,
	book.InUse,
	(select
	stuff((
			select ','+g.Name from BookGenres bg 
			left join Genres g on bg.GenreId=g.Id 
			where bg.BookId=book.Id
			FOR XML path('')
		),1,1,'')) as Genre,
	(select
		stuff((
			select ','+a.Name from BookAuthors ba 
			left join Authors a on ba.AuthorId=a.Id 
			where ba.BookId=book.Id
			FOR XML path('')
		),1,1,'')) as Author
	from Books book
	left join PublishingHouses ph on book.PublishingHouseId=ph.Id
	RETURN
END



ALTER FUNCTION [dbo].[SearchBooks]
(
	@Name nvarchar(250),
	@Author nvarchar(250)
)
RETURNS @returntable TABLE
(
	Id int,
	Name nvarchar(250),
	ShortDescription nvarchar(250),
	Genre nvarchar(250),
	[Count] int,
	Pic varbinary(max),
	InUse int,
	Authors nvarchar(250)
)
AS
BEGIN
	INSERT @returntable (Id,Name,ShortDescription,[Count],Pic,InUse,Genre,Authors)
	select tbl.* from (
	select 
	book.Id,
	book.BookName,
	book.ShortDescription,
	book.[Count],
	book.Pic,
	book.InUse,
	(select
	stuff((
			select ','+g.Name from BookGenres bg 
			left join Genres g on bg.GenreId=g.Id 
			where bg.BookId=book.Id
			FOR XML path('')
		),1,1,'')) as Genre,
	(select
		stuff((
			select ','+a.Name from BookAuthors ba 
			left join Authors a on ba.AuthorId=a.Id 
			where ba.BookId=book.Id
			FOR XML path('')
		),1,1,'')) as Author
	from Books book) tbl
	where 
	tbl.BookName like '%'+ISNULL(@Name,'')+'%'
	or tbl.Author like '%'+ISNULL(@Author,'')+'%'
	or CONTAINS((tbl.BookName,tbl.Author,tbl.Genre,tbl.ShortDescription),@Name)
	RETURN
END


*/