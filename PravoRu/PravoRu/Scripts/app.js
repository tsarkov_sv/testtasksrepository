﻿$('body').on('click','#searchBtn',function () {
    $.post('/view/Books', { Name: $('#Filter').val(), Search: $('#SearchValue').val() })
    .done(function (data) {
        $('#Books').empty();
        $('#Books').append(data);
    });
});

$('body').on('click', '#reportTopBtn', function () {
    $('#headerReport').text('Топ 10 книг');
    $('#ReportTop').empty()
    $.post('/view/Top',null)
    .done(function (data) {
        if (data)
            $('#ReportTop').append(data);
        else 
            $('#ReportTop').append("Отчет отсутствует");
    });
});

$('body').on('click', '#reportChartBtn', function () {
    $('#headerReport').text('Статистика');
    $('#ReportTop').empty()
    $.post('/view/Chart', { StartDateStr: $('#StartDate').val(), EndDateStr: $('#EndDate').val() })
    .done(function (data) {
        if (data)
            $('#ReportTop').append(data);
        else
            $('#ReportTop').append("Отчет отсутствует");
    });
});

